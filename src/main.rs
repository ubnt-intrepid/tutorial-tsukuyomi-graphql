#[macro_use]
extern crate juniper;
extern crate tsukuyomi;
extern crate tsukuyomi_juniper;
#[macro_use]
extern crate failure;
extern crate chrono;
extern crate uuid;

mod context;
mod schema;

use tsukuyomi::App;
use tsukuyomi_juniper::{AppGraphQLExt as _AppExt, GraphQLState};

fn main() -> tsukuyomi::AppResult<()> {
    let context = context::create_context();
    let schema = schema::create_schema();
    let state = GraphQLState::new(context, schema);

    let app = App::builder()
        .graphql("/graphql", state)
        .graphiql("/graphiql", "http://127.0.0.1:4000/graphql")
        .finish()?;

    tsukuyomi::run(app)
}
