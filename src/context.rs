use juniper;
use std::sync::RwLock;
use uuid::Uuid;

use schema::{AddTodoInput, EditTodoInput, Todo};

pub type Result<T> = ::std::result::Result<T, ::failure::Error>;

#[derive(Debug)]
pub struct Context(RwLock<Inner>);

#[derive(Debug)]
struct Inner {
    todos: Vec<Todo>,
}

impl juniper::Context for Context {}

impl Context {
    pub fn load_all(&self) -> Result<Vec<Todo>> {
        self.with_read(|cx| cx.todos.clone())
    }

    pub fn load(&self, id: Uuid) -> Result<Option<Todo>> {
        self.with_read(|cx| cx.todos.iter().find(|todo| todo.id == id).cloned())
    }

    pub fn add_todo(&self, input: AddTodoInput) -> Result<Uuid> {
        self.with_write(|cx| {
            let id = Uuid::new_v4();
            let new_todo = Todo {
                id: id,
                text: input.text,
                priority: input.priority,
                due_date: input.due_date,
                completed: false,
            };
            cx.todos.push(new_todo);
            id
        })
    }

    pub fn edit_todo(&self, id: Uuid, input: Option<EditTodoInput>) -> Result<Option<Todo>> {
        self.with_write(|cx| {
            cx.todos.iter_mut().find(|todo| todo.id == id).map(|todo| {
                if let Some(input) = input {
                    if let Some(text) = input.text {
                        todo.text = text;
                    }
                    if let Some(priority) = input.priority {
                        todo.priority = Some(priority);
                    }
                    if let Some(due_date) = input.due_date {
                        todo.due_date = Some(due_date);
                    }
                    if let Some(completed) = input.completed {
                        todo.completed = completed;
                    }
                };
                todo.clone()
            })
        })
    }

    pub fn delete_todo(&self, id: Uuid) -> Result<i32> {
        self.with_write(|cx| {
            let new_todos: Vec<_> = cx.todos.drain(..).filter(|todo| todo.id != id).collect();
            let num_affected_rows = cx.todos.len() - new_todos.len();
            cx.todos = new_todos;
            num_affected_rows as i32
        })
    }

    fn with_read<R>(&self, f: impl FnOnce(&Inner) -> R) -> Result<R> {
        let inner = self.0.read().map_err(|e| format_err!("{}", e))?;
        Ok(f(&*inner))
    }

    fn with_write<R>(&self, f: impl FnOnce(&mut Inner) -> R) -> Result<R> {
        let mut inner = self.0.write().map_err(|e| format_err!("{}", e))?;
        Ok(f(&mut *inner))
    }
}

pub fn create_context() -> Context {
    Context(RwLock::new(Inner { todos: vec![] }))
}
