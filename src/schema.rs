use chrono::{DateTime, Utc};
use juniper::{FieldResult, RootNode};
use uuid::Uuid;

use context::Context;

pub type TimeStamp = DateTime<Utc>;

#[derive(Debug, Copy, Clone, GraphQLEnum)]
pub enum Priority {
    Low,
    Medium,
    High,
}

#[derive(Debug, Clone, GraphQLObject)]
pub struct Todo {
    pub id: Uuid,
    pub text: String,
    pub priority: Option<Priority>,
    pub due_date: Option<TimeStamp>,
    pub completed: bool,
}

#[derive(Debug)]
pub struct Query {
    _priv: (),
}

graphql_object!(Query: Context |&self| {
    field apiVersion() -> &'static str {
        "1.0"
    }

    field todos(&executor) -> FieldResult<Vec<Todo>> {
        executor.context().load_all().map_err(Into::into)
    }

    field todo(&executor, id: Uuid) -> FieldResult<Option<Todo>> {
        executor.context().load(id).map_err(Into::into)
    }
});

#[derive(Debug, GraphQLInputObject)]
pub struct AddTodoInput {
    pub text: String,
    pub priority: Option<Priority>,
    pub due_date: Option<TimeStamp>,
}

#[derive(Debug, GraphQLInputObject)]
pub struct EditTodoInput {
    pub text: Option<String>,
    pub priority: Option<Priority>,
    pub due_date: Option<TimeStamp>,
    pub completed: Option<bool>,
}

#[derive(Debug)]
pub struct Mutation {
    _priv: (),
}

graphql_object!(Mutation: Context |&self| {
    field addTodo(&executor, todo: AddTodoInput) -> FieldResult<Uuid> {
        executor.context().add_todo(todo).map_err(Into::into)
    }

    field editTodo(&executor, id: Uuid, todo: Option<EditTodoInput>) -> FieldResult<Option<Todo>> {
        executor.context().edit_todo(id, todo).map_err(Into::into)
    }

    field deleteTodo(&executor, id: Uuid) -> FieldResult<i32> {
        executor.context().delete_todo(id).map_err(Into::into)
    }
});

pub type Schema = RootNode<'static, Query, Mutation>;

pub fn create_schema() -> Schema {
    RootNode::new(Query { _priv: () }, Mutation { _priv: {} })
}
